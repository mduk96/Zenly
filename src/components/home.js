import { Link } from 'react-router-dom';
import '../css/main.scss';
import { auth, database } from '../firebase';
import { useHistory } from "react-router";
import { useEffect, useRef, useState } from 'react';

const GOOGLE_API_KEY = 'AIzaSyDD1bL9fKZ3r1YsNSBNd7kWwVyW3F4FkV4';
const LAT = 47.9186349;
const LONG = 106.9153912;
const ZOOM = 15;

const Home = ({ currentUser }) => {
  const [markers, setMarkers] = useState([]);
  const mapRef = useRef(null);
  const mapElementRef = useRef(null);
  const history = useHistory();

  const signOut = () => {
    database.collection('locations').doc(currentUser.id).update({ status: 'offline' });
    auth.signOut();
  };
  
  const initMap = () => {
    mapRef.current = new window.google.maps.Map(mapElementRef.current, {
      center: { lat: LAT, lng: LONG },
      zoom: ZOOM
    });
  };

  const getData = async(snapshot) => {
    try {
      const temp = [];
      snapshot.forEach(doc => {
        if(doc.exists){
          temp.push({ id: doc.id, ...doc.data() });
        };
      });
      setMarkers(old => {
        return temp.map(item => {
          return new window.google.maps.Marker({
            position: { lat: item.lat, lng: item.lng },
            map: mapRef.current,
          });
        });
      });
    } catch(err) {
      console.error(err.message);
    };
  };
  
  useEffect(() => {
    // Init google map
    const googleMapScript = document.createElement('script');
    googleMapScript.src = `https://maps.googleapis.com/maps/api/js?key=${ GOOGLE_API_KEY }&libraries=places`;
    googleMapScript.async = true;
    window.document.body.appendChild(googleMapScript);
    googleMapScript.addEventListener('load', initMap);

    // Get friends' location
    database.collection('locations').onSnapshot(getData);
  }, []);

  if (currentUser === null) history.replace('/login');

  return (
    <div className='relative w-full h-full'>
      <div ref={ mapElementRef } className='absolute w-full h-full'></div>
      <header className='absolute top-0 left-0 p-4 w-full flex justify-between items-center'>
        <button className='w-16 h-16 bg-white rounded-3xl shadow-md bg-opacity-50 bg-no-repeat bg-contain bg-center focus:outline-none' style={{ backgroundImage: 'URL(/assets/database.svg)' }}></button>
        <div className='text-4xl font-extrabold clr--indigo uppercase'>location</div>
        <button onClick={ signOut } className='w-16 h-16 bg-white rounded-3xl shadow-md bg-opacity-50 bg-no-repeat bg-contain bg-center focus:outline-none' style={{ backgroundImage: 'URL(/assets/tiara.svg)' }}></button>
      </header>
      {
        false && <>
              <button className='absolute top-20 right-20 w-10 h-10 bg--blue rounded-full focus:outline-none' style={{ boxShadow: '0px 0px 1px 8px white', }}></button>
              <button className='absolute top-1/3 right-1/2 w-16 h-16 bg--blue bg-no-repeat bg-center bg-cover rounded-full focus:outline-none' style={{ boxShadow: '0px 0px 1px 12px rgb(201 246 113)', backgroundImage: 'URL(null)' }}></button>
            </>
      }
      <footer className='absolute bottom-10 left-0 p-4 w-full flex flex-col items-center'>
        <button className='w-20 h-20 mb-8 bg-white rounded-3xl shadow-lg bg-opacity-50 bg-no-repeat bg-contain bg-center focus:outline-none' style={{ backgroundImage: 'URL(/assets/globe.svg)', }}></button>
        <div className='w-full flex justify-between'>
          <Link to='/chat'  className='w-16 h-16 bg-white rounded-3xl shadow-md bg-opacity-50 bg-no-repeat bg-contain bg-center focus:outline-none' style={{ backgroundImage: 'URL(/assets/chat.svg)' }}></Link>
          <Link to='/contact' className='w-16 h-16 bg--pink rounded-3xl shadow-md bg-no-repeat bg-contain bg-center focus:outline-none' style={{ backgroundImage: 'URL(/assets/plus.svg)' }}></Link>
          <Link to='/profile' className='w-16 h-16 bg-white rounded-3xl shadow-md bg-opacity-50 bg-no-repeat bg-contain bg-center focus:outline-none' style={{ backgroundImage: 'URL(/assets/user.svg)' }}></Link>
        </div>
      </footer>
    </div>
  );
}
  
export default Home; 