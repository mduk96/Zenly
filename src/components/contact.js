import '../css/main.scss';
import { useHistory } from "react-router";


const PerContact = (props) => {
  
  return (
    <div className='mt-4 flex items-center gap-4'>
      <div className='w-16 h-16 rounded-3xl flex justify-center items-center text-2xl font-bold bg--gray-10 clr-gray-20'>T</div>
      <div className='flex-1 flex flex-col justify-between'>
        <p className='clr--indigo font-bold text-xl'>Tselmeg</p>
        <span  className='clr--gray-20'>1 friend on Zenly</span>
      </div>
      <button className='pl-10 pr-4 py-2 rounded-xl text-white font-bold bg--pink bg-no-repeat bg-left' style={{ backgroundImage: 'URL(assets/plus.svg)', backgroundSize: '30px', backgroundPositionX: '10px' }}>ADD</button>
      <button className='w-8 h-8 bg-no-repeat bg-center bg-contain' style={{ backgroundImage: 'URL(assets/x.svg)' }}></button>
    </div>
  );
};

const Contact = ({ currentUser }) => {
  const history = useHistory();
  
  if (currentUser === null) history.replace('/login');

  return (
    <div className='p-4 w-full h-full bg-white'>
      <header>
        <div className='w-full flex justify-between gap-4'>
          <h1 className='text-3xl font-extrabold clr--indigo'>FRIENDS</h1>
          <button className='pl-10 pr-4 py-2 rounded-xl bg--pink text-white font-bold bg-no-repeat bg-left' style={{ backgroundImage: 'URL(assets/plus.svg)', backgroundSize: '30px', backgroundPositionX: '10px' }}>ADD FRIENDS</button>
        </div>
        { true 
          ? <button className='mt-4 w-full bg--gray-10 px-8 py-4 rounded-xl font-semibold clr--indigo focus:outline-none'>SEARCH</button>
          : <div className='mt-4 w-full bg--gray-10 px-8 py-4 flex justify-between rounded-xl gap-2'>
              <input className='flex-1 bg-transparent clr--indigo focus:outline-none placeholder--indigo' placeholder='SEARCH' />
              <button className='py-1.5 px-3 font-semibold rounded-lg bg--indigo clr--blue'>CANCEL</button>
            </div>
        }
      </header>
      <section className='my-8'>
        <h2 className='text-xl font-bold clr--gray-20'>YOU MIGHT KNOW THEM</h2>
        <PerContact />
      </section>
    </div>
  );
}
  
export default Contact; 