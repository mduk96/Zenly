import { Link } from 'react-router-dom';
import '../css/main.scss';
import { useHistory } from "react-router";
import { useRef, useState } from 'react';
import { storageRef, database } from '../firebase';


const Profile = ({ currentUser, setCurrentUser }) => {
  const history = useHistory();
  const imageRef = useRef(null);
  const inputRef = useRef(null);
  const [isInputDisabled, setIsInputDisabled] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const setImage = async(e) => {
    const file = e.target.files[0];
    if(!file) return;
    setIsLoading(true);

    try {
        const response = await storageRef.child('profile/' + currentUser.id).put(file);
        const url = await response.ref.getDownloadURL();

        await database.collection('users').doc(currentUser.id).update({ img: url });
        setCurrentUser(old => {
          const update = {...old};
          update.img = url;
          return update;
        });
    } catch(err) {
        console.error(err.message);
    };

    setIsLoading(false);
  };
  const setName = async(e) => {
    const value = e.target.value.trim();
    if(!value) return;

    try {
      await database.collection('users').doc(currentUser.id).update({ name: value });
      setCurrentUser(old => {
        const update = {...old};
        update.name = value;
        return update;
      });
    } catch(err) {
      console.error(err.message);
    };
  };

  if (currentUser === null) history.replace('/login');
    
  return (
    <div className='w-full h-full flex'>
      <div className='relative mt-8 p-4 flex-1 flex flex-col bg-white rounded-t-2xl shadow overflow-hidden clr--gray-10'>
        <header>
          <div className='flex gap-4'>
            <span className='w-10 h-10 bg-no-repeat bg-contain bg-center' style={{ backgroundImage: 'URL(/assets/settings.svg)' }}></span>
            <span className='w-10 h-10 bg-no-repeat bg-contain bg-center' style={{ backgroundImage: 'URL(/assets/ghost.svg)' }}></span>
          </div>
          <div className='pt-4 flex justify-between'>
            <div className='flex flex-col justify-between'>
              <input className='w-60 bg-red-200 text-4xl font-bold clr--indigo' ref={ inputRef } type='text' defaultValue={ currentUser.name || 'Your name' } disabled={ isInputDisabled } />
              <button className='mt-4 pl-10 px-4 py-1.5 rounded-2xl bg-no-repeat bg-contain bg--gray-10 clr--gray-20' style={{ backgroundImage: 'URL(/assets/pencil.svg)', backgroundPosition: '10px', backgroundSize: '25px', }}>SET USERNAME</button>
            </div>
            <div className='relative w-20 h-20 rounded-3xl flex justify-center items-center text-2xl font-bold bg--gray-10 clr-gray-20' style={{ backgroundImage: `URL(${ currentUser.img })`, }}>
              { isLoading && <div className="loader"></div> }
              <label htmlFor='file' className='absolute bottom-0 right-0 border-4 border-white w-8 h-8 rounded-xl bg-no-repeat bg-center bg-contain bg--gray-10 focus:outline-none' style={{ backgroundImage: 'URL(/assets/camera.svg)', }}></label>
              <input onChange={ setImage } id='file' type='file' accept='image/*' className='hidden' />
            </div>
          </div>
        </header>
        <section className='pt-10 flex-1 flex flex-col'>
          <h2 className='text-2xl font-bold clr--gray-20'>HIGHLIGHTS</h2>
          <div className='mt-4 flex gap-4'>
            <div className='p-2 flex-1 bg--gray-10 rounded-xl'>
              <h4 className='font-medium'>Requests</h4>
              <span className='text-4xl font-light clr--indigo'>0</span>
            </div>
            <div className='p-2 flex-1 bg--gray-10 rounded-xl'>
              <h4 className='font-medium'>Friends</h4>
              <span className='text-4xl font-light clr--indigo'>0</span>
            </div>
          </div>
          <div className='mt-4 px-4 py-2 bg--gray-10 rounded-xl'>
            <h4 className='font-medium'>CHANGE YOUR APP ICON</h4>
            <div className='p-2 flex justify-around gap-2'>
              <div className='w-16 h-16 rounded-2xl bg-no-repeat bg-center bg--gray-20' style={{ backgroundImage: 'URL(/assets/lock.svg)', backgroundSize: '25px', }}></div>
              <div className='w-16 h-16 rounded-2xl bg-no-repeat bg-center bg--gray-20' style={{ backgroundImage: 'URL(/assets/lock.svg)', backgroundSize: '25px', }}></div>
              <div className='w-16 h-16 rounded-2xl bg-no-repeat bg-center bg--gray-20' style={{ backgroundImage: 'URL(/assets/lock.svg)', backgroundSize: '25px', }}></div>
              <div className='w-16 h-16 rounded-2xl bg-no-repeat bg-center bg--gray-20' style={{ backgroundImage: 'URL(/assets/lock.svg)', backgroundSize: '25px', }}></div>
            </div>
          </div>
          <div className='mb-28 flex-1 flex gap-4 flex-col justify-center items-center text-center'>
            <div className='w-20 h-20 flex justify-center items-center text-white font-semibold bg-no-repeat bg-contain bg-center' style={{ backgroundImage: 'URL(/assets/heart.svg)' }}>Z+D</div>
            <p className='max-w-md'>In a stroke of genius, you joined Zenly on a bright morning in May 2021. It's been 6 days since then and our love for you has never faltered.</p>
          </div>
        </section>
        <footer className='absolute bottom-10 w-full flex justify-evenly items-center'>
          <Link to='/chat'  className='w-16 h-16 bg-white rounded-3xl shadow-md bg-opacity-50 bg-no-repeat bg-contain bg-center focus:outline-none' style={{ backgroundImage: 'URL(/assets/chat.svg)' }}></Link>
          <Link to='/' className='w-16 h-16 bg-white rounded-3xl shadow-lg bg-opacity-50 bg-no-repeat bg-contain bg-center focus:outline-none' style={{ backgroundImage: 'URL(/assets/globe.svg)' }}></Link>
          <Link to='/profile' className='w-20 h-20 bg-white rounded-3xl shadow-2xl bg-opacity-50 bg-no-repeat bg-contain bg-center focus:outline-none' style={{ backgroundImage: 'URL(/assets/user.svg)' }}></Link>
        </footer>
      </div>
    </div>
  );
}
  
export default Profile; 