import { Logo } from '../assets/svg';

const LoaderPage = () => {
    
    return (
      <div className='mx-auto w-full h-screen max-h-screen bg--blue shadow-lg flex justify-center items-center' style={{ maxWidth: '600px' }}>
        <Logo size='lg'/>
      </div>
    );
}
  
export default LoaderPage;