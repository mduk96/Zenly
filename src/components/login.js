import { Logo } from '../assets/svg';
import firebase, { auth } from '../firebase';
import '../css/main.scss';
import { useRef, useState } from 'react';
import { useHistory } from 'react-router';


const Verification = ({ setPageNum }) => {
  const inputRef = useRef(null);
  const [error, setError] = useState(null);

  const handleSubmit = async(e) => {
    e.preventDefault();
    
    const temp = inputRef.current.value.trim();
    if(!temp) return;

    try {
      await window.confirmationResult.confirm(temp);
    } catch (err) {
      console.error('verification ' + err);
      setError(err.message);
    };
  };

  return (
    <form onSubmit={ handleSubmit } className='relative p-4 w-full h-full flex flex-col justify-center items-center text-white'>
      { error && <div className='w-full mb-5 bg-red-50 p-2 border rounded-sm border-red-200 text-red-500 text-center'>{ error }</div> }
      <label className='absolute top-4 left-4 uppercase text-4xl font-bold'>code please</label>
      <input ref={ inputRef } type='code' placeholder='Your Code' className='bg-transparent outline-none text-center text-4xl font-bold placeholder-blue-800'></input>
      <button type='submit' className='absolute bottom-4 right-4 py-2 px-4 rounded-2xl font-bold bg--pink focus:outline-none'>-></button>
    </form>
  );
};

const SentCode = ({ setPageNum }) => {
  const inputRef = useRef(null);
  const [error, setError] = useState(null);
  
  const handleSubmit = async(e) => {
    e.preventDefault();
    
    const temp = inputRef.current.value.trim();
    if(!temp) return;

    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
      'size': 'invisible'
    });
    
    const appVerifier = window.recaptchaVerifier;
    try {
        window.confirmationResult = await auth.signInWithPhoneNumber(`+976 ${ temp }`, appVerifier);
        setError(null);
        inputRef.current.value = '';
        setPageNum(old => old + 1);
    } catch (err) {
        console.error('sentcode ' + err);
        setError(err.message);
    };
  };

  return (
    <form onSubmit={ handleSubmit } className='relative p-4 w-full h-full flex flex-col justify-center items-center text-white'>
      { error && <div className='w-full mb-5 bg-red-50 p-2 border rounded-sm border-red-200 text-red-500 text-center'>{ error }</div> }
      <label className='absolute top-4 left-4 uppercase text-4xl font-bold'>i need your phone number to identify you</label>
      <input ref={ inputRef } type='tel' placeholder='Your Phone' className='bg-transparent outline-none text-center text-4xl font-bold placeholder-blue-800'></input>
      <button id='sign-in-button' type='submit' className='absolute bottom-4 right-4 py-2 px-4 rounded-2xl font-bold bg--pink focus:outline-none'>-></button>
    </form>
  );
};

const Login = ({ currentUser }) => {
  const [pageNum, setPageNum] = useState(0);
  const history = useHistory();

  if(currentUser) history.replace('/');
  if(pageNum === 1) return <SentCode setPageNum={ setPageNum }/>;
  if(pageNum === 2) return <Verification setPageNum={ setPageNum }/>;

  return (
    <div className='relative p-4 w-full h-full flex flex-col justify-center items-center text-center text-white'>
      <div className='flex items-center'>
        <Logo size='sm'/>
        <h1 className='pl-2 text-7xl font-bold'>zenly</h1>
      </div>
      <h2 className='mt-4 text-2xl font-bold uppercase'>your map, your people</h2>
      <div className='absolute bottom-10 flex flex-col items-center'>
        <p className='w-3/4'>By tapping on "Sign up & Accept", you agree to the Privacy Policy and Terms of Services</p>
        <button onClick={ () => setPageNum(1) } className='mt-8 py-2 px-4 max-w-max rounded-2xl uppercase bg--pink focus:outline-none'>sign up & accept</button>
      </div>
    </div>
  );
};
  
export default Login;
  