import { useHistory } from "react-router";


const Chat = ({ currentUser }) => {
  const history = useHistory();
  
  if (currentUser === null) history.replace('/login');

  return (
    <div>
      Chat
    </div>
  );
}
  
export default Chat; 