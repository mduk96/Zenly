import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

// Your web app's Firebase configuration
// console.log(process.env);
const firebaseConfig = {
    apiKey: "AIzaSyBb8KLrjAGNrVC2f-Vt3TbYXM9fxzgIjfg",
    authDomain: "zenly-52359.firebaseapp.com",
    projectId: "zenly-52359",
    storageBucket: "zenly-52359.appspot.com",
    messagingSenderId: "363351638492",
    appId: "1:363351638492:web:173d1548670a0c04f00f19",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;
export const auth = firebase.auth();
export const database = firebase.firestore();
export const storageRef = firebase.storage().ref(); 