import { Route, Switch } from "react-router";
import { useEffect, useState, useRef } from 'react';
import Home from './components/home';
import Login from './components/login';
import Profile from './components/profile';
import Contact from './components/contact';
import Chat from './components/chat';
import LoaderPage from './components/loader';
import { auth, database } from './firebase';


const App = () => {
  const [currentUser, setCurrentUser] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const locationWatcherRef = useRef(null);
  const locationRef = useRef(null);

  const getUser = async(user) => {
    setIsLoading(true);
    if (user) {
      const response = await database.collection('users').doc(user.uid).get();
      let temp = {
        ...response.data(),
        id: user.uid,
        phone: user.phoneNumber,
      };
      setCurrentUser(temp);
    } else {
      setCurrentUser(null);
    };
    setIsLoading(false);
  };
  
  const getNavigation = (loc) => {
    console.log(loc);
    let temp = {
      date: new Date(loc.timestamp),
      lat: loc.coords.latitude,
      lng: loc.coords.longitude,
      status: 'online',
    };
    
    if(!locationRef.current){
      locationRef.current = temp;
      database.collection('locations').doc(currentUser.id).set(locationRef.current);
      return;
    };

    
  };

  useEffect(() => {
    // Navigator
    if(currentUser){
      locationWatcherRef.current = navigator.geolocation.watchPosition(getNavigation, console.error, { maximumAge: 60000 });
    } else {
      if(locationWatcherRef.current) navigator.geolocation.clearWatch(locationWatcherRef.current);
    };

    return () => {
      if(locationWatcherRef.current) navigator.geolocation.clearWatch(locationWatcherRef.current);
      if(currentUser) database.collection('locations').doc(currentUser.id).set({ status: 'offline' }, { merge: true });
    };
  }, [currentUser]);

  useEffect(() => {
    // User check
    const unsubscribe = auth.onAuthStateChanged(getUser);

    return () => {
      unsubscribe();
    };
  }, []);

  if (isLoading) return <LoaderPage />;

  return (
    <main className='mx-auto w-full h-screen max-h-screen bg--blue shadow-lg' style={{ maxWidth: '600px' }}>
      <Switch>
        <Route exact path="/" component={ () => <Home currentUser={ currentUser } /> } />
        <Route path="/login" component={ () => <Login currentUser={ currentUser } /> } />
        <Route path="/profile" component={ () => <Profile currentUser={ currentUser } setCurrentUser={ setCurrentUser } /> } />
        <Route path="/contact" component={ () => <Contact currentUser={ currentUser } /> } />
        <Route path="/chat" component={ () => <Chat currentUser={ currentUser } /> } />
      </Switch>
    </main>
  );
}

export default App;
